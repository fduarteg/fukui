# fukui
The Fukui function allows one to predict, using density functional theory, where the most electrophilic and nucleophilic sites of a molecule are. It indicates the change in electron density of a molecule at a given position when the number of electrons have been changed. The condensed Fukui function is the same idea, but applied to an atom within a molecule, rather than a point in three-dimensional space.

#Calculation
The function itself can be quantified mathematically as follows:
![my equation](https://latex.codecogs.com/gif.download?%5Czeta%28s%29%20%3D%20%5Csum_%7Bn%3D1%7D%5E%5Cinfty%20%5Cfrac%7B1%7D%7Bn%5Es%7D)
The Fukui function itself has two finite versions of this change which can be defined by the following two functions. The form of the function will depend on whether or not an electron was removed or added from the molecule. 

The Fukui function for the addition of an electron to a molecule is as follows:

{\displaystyle f_{+}(r)=\rho _{N+1}({\textbf {r}})-\rho _{N}({\textbf {r}})} {\displaystyle f_{+}(r)=\rho _{N+1}({\textbf {r}})-\rho _{N}({\textbf {r}})}.

The next function will represent the Fukui function in terms of the removal of an electron from the molecule:

{\displaystyle f_{-}(r)=\rho _{N}({\textbf {r}})-\rho _{N-1}({\textbf {r}})} {\displaystyle f_{-}(r)=\rho _{N}({\textbf {r}})-\rho _{N-1}({\textbf {r}})}.

The {\displaystyle f_{+}} {\displaystyle f_{+}} function represents the initial part of a nucleophilic reaction. The {\displaystyle f_{-}} {\displaystyle f_{-}}, on the other hand, represents the initial part of an electrophilic reaction. The reaction will therefore then take place where the {\displaystyle f_{\pm }} f_{\pm } can be found to have a large value. Solving for either Fukui function would result in a representation of the molecule's electron density for either electrophilicity or nucleophilicity.[5]

